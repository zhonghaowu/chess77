package piece;

import chess.Board;
import chess.Chess;
import chess.Player;
import chess.Assembly;

/**
 *  This is the abstract class 
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public abstract class Piece {
	/**
	 * location of the piece
	 */
	public int[] loc;
	/**
	 * color of the piece
	 */
	public String color;
	/**
	 * owner of the piece
	 */
	public Player owner;
	/**
	 * piece type, it can only be king, queen, bishop, knight, rook, pawn
	 */
	public String type;
	/**
	 * counter for judge if a piece have moved befor
	 */
	public int counter = 0;
	// status:0	dead, wait to be cleared 
	// status:1 live
	/**
	 * all locs which can be moved are given true(including piece eating loc)
	 * @param board chess board
	 * @return matrix of legal move
	 */
	public abstract boolean[][] legalMove(Board board);
	
	/**
	 * make piece move (may capture enemy),
	 * if the move is valid
	 * @param board chess board
	 * @param end target coordinate
	 * @return boolean if the piece can move legally
	 */
	public boolean move(Board board, int[] end) {
		// TODO Auto-generated method stub
		if(!Assembly.inMatrix(end))	return false;
		boolean[][] available = this.legalMove(board);
		if(!available[end[0]][end[1]])	return false;
		if(!this.king_safe_check(board, end))	return false;
		board.board[end[0]][end[1]] = this;
		board.board[loc[0]][loc[1]] = null;
		this.loc = end;
		return true;
	}
	
	/**
	 * 
	 * @param color color of the piece
	 * @param owner owner of the piece
	 * @param loc spawn location of the piece
	 * @param type piece type of the piece
	 */
	public Piece(String color, Player owner, int[] loc, String type) {
		this.color = color;
		this.owner = owner;
		this.loc = loc;
		this.type = type;
	}
	
	/**
	 * 
	 * @param board chess board
	 * @param end target coordinate
	 * @return true if and only if king is safe
	 */
	public boolean king_safe_check(Board board, int[] end) {
		Board copy = board.getCopy();
		int[] oriloc = this.loc;	//store original loc
		copy.board[end[0]][end[1]] = this;
		copy.board[oriloc[0]][oriloc[1]] = null;
		this.loc = end;
		boolean flag = true;
		King k = Chess.getKing(this.owner, board);
		if(k.check_detection(copy, k.loc)) flag = false;
		this.loc = oriloc;
		return flag;
	}
	
    /**
     * check if Rook has been moved
     * @return boolean has been moved
     */
    public boolean isMoved() {
        if (counter != 0) return true;
        return false;
    }
}

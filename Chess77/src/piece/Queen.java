package piece;

import java.util.Arrays;

import chess.Board;
import chess.Player;
import chess.Assembly;


/**
 * This is the Queen 
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Queen extends Piece{
	/**
	 * 
	 * @param color color of queen
	 * @param owner owner of queen
	 * @param loc location of queen
	 * @param type must be queen
	 */
	public Queen(String color, Player owner, int[] loc, String type) {
		super(color, owner, loc, type);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean[][] legalMove(Board board) {
		// TODO Auto-generated method stub
		//scan down
		boolean[][] ret = new boolean[8][8];
		for(int i = loc[0] + 1; i < 8; i++) {
			int[] target = new int[] {i, loc[1]};
			if (!checkValidity(target, board, ret))	break;
		}
		//scan up
		for(int i = loc[0] - 1; i >= 0; i--) {
			int[] target = new int[] {i, loc[1]};
			if (!checkValidity(target, board, ret))	break;
		}
		//scan right
		for(int i = loc[1] + 1; i < 8; i++) {
			int[] target = new int[] {loc[0],i};
			if (!checkValidity(target, board, ret))	break;
		}
		//scan left
		for(int i = loc[1] - 1; i >= 0; i--) {
			int[] target = new int[] {loc[0],i};
			if (!checkValidity(target, board, ret))	break;
		}
		for(int x = loc[0]-1, y = loc[1]+1; x >= 0 && y < 8; x--, y++) {
			int[] target = new int[] {x,y};
			if (!checkValidity(target, board, ret))	break;
		}
		for(int x = loc[0]-1, y = loc[1]-1; x >= 0 && y >= 0; x--, y--) {
			int[] target = new int[] {x,y};
			if (!checkValidity(target, board, ret))	break;
		}
		for(int x = loc[0]+1, y = loc[1]+1; x < 8 && y < 8; x++, y++) {
			int[] target = new int[] {x,y};
			if (!checkValidity(target, board, ret))	break;
		}
		for(int x = loc[0]+1, y = loc[1]-1; x < 8 && y >= 0; x++, y--) {
			int[] target = new int[] {x,y};
			if (!checkValidity(target, board, ret))	break;
		}
		return ret;
	}
	
	/**
	 * check if queen can move to a coordinate
	 * @param target
	 * @param board
	 * @param ret
	 * @return boolean can move to coordinate
	 */
	private boolean checkValidity(int[] target, Board board, boolean[][] ret) {
		if(board.get(target) != null && (board.get(target).color.equals(this.color)))	return false;
		ret[target[0]][target[1]] = true;
		if(board.get(target) != null && !(board.get(target).color.equals(this.color)))	return false;
		return true;
	}
	/**
	 * @return string name
	 */
	public String toString() {
		return this.color.charAt(0) + "Q";
	}
	
}

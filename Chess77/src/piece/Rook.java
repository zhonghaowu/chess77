package piece;
import chess.Board;
import chess.Player;
import chess.Assembly;

/**
 * This is the Rook 
 * @author Zhonghao Wu Thomas Cooke
 *
 */

public class Rook extends Piece{
	/**
	 * 
	 * @param color color of rook
	 * @param owner owner of rook
	 * @param loc location of rook
	 * @param type must be rook
	 */
    public Rook(String color, Player owner, int[] loc, String type) {
        super(color, owner, loc, type);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean[][] legalMove(Board board) {
        // TODO Auto-generated method stub
        //scan down
        boolean[][] ret = new boolean[8][8];
        for(int i = loc[0] + 1; i < 8; i++) {
            int[] target = new int[] {i, loc[1]};
            if (!checkValidity(target, board, ret))	break;
        }
        //scan up
        for(int i = loc[0] - 1; i >= 0; i--) {
            int[] target = new int[] {i, loc[1]};
            if (!checkValidity(target, board, ret))	break;
        }
        //scan right
        for(int i = loc[1] + 1; i < 8; i++) {
            int[] target = new int[] {loc[0],i};
            if (!checkValidity(target, board, ret))	break;
        }
        //scan left
        for(int i = loc[1] - 1; i >= 0; i--) {
            int[] target = new int[] {loc[0],i};
            if (!checkValidity(target, board, ret))	break;
        }

        return ret;
    }

    /**
     * check if Rook can move to a coordinate
     * @param target
     * @param board
     * @param ret
     * @return boolean can move to coordinate
     */
    private boolean checkValidity(int[] target, Board board, boolean[][] ret) {
        if(board.get(target) != null && (board.get(target).color.equals(this.color)))	return false;
        ret[target[0]][target[1]] = true;
        if(board.get(target) != null && !(board.get(target).color.equals(this.color)))	return false;
        return true;
    }

    /**
     * check if Rook has been moved
     * @return boolean has been moved
     */
    public boolean isMoved() {
        if (counter != 0) return true;
        return false;
    }

    /**
     * @return string name
     */
    public String toString() {
        return this.color.charAt(0) + "R";
    }


}
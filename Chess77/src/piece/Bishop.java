package piece;
import chess.Board;
import chess.Player;
import chess.Assembly;

/**
 * This is the Bishop 
 * @author Zhonghao Wu Thomas Cooke
 *
 */

public class Bishop extends Piece{
	
	/**
	 * 
	 * @param color color of bishop
	 * @param owner owner of bishop
	 * @param loc location of bishop
	 * @param type must be bishop
	 */
    public Bishop(String color, Player owner, int[] loc, String type) {
        super(color, owner, loc, type);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean[][] legalMove(Board board) {
        // TODO Auto-generated method stub
        boolean[][] ret = new boolean[8][8];
        for(int x = loc[0]-1, y = loc[1]+1; x >= 0 && y < 8; x--, y++) {
            int[] target = new int[] {x,y};
            if (!checkValidity(target, board, ret))	break;
        }
        for(int x = loc[0]-1, y = loc[1]-1; x >= 0 && y >= 0; x--, y--) {
            int[] target = new int[] {x,y};
            if (!checkValidity(target, board, ret))	break;
        }
        for(int x = loc[0]+1, y = loc[1]+1; x < 8 && y < 8; x++, y++) {
            int[] target = new int[] {x,y};
            if (!checkValidity(target, board, ret))	break;
        }
        for(int x = loc[0]+1, y = loc[1]-1; x < 8 && y >= 0; x++, y--) {
            int[] target = new int[] {x,y};
            if (!checkValidity(target, board, ret))	break;
        }

        return ret;
    }

    /**
     * check if Bishop can move to a coordinate
     * @param target
     * @param board
     * @param ret
     * @return boolean can move to coordinate
     */
    private boolean checkValidity(int[] target, Board board, boolean[][] ret) {
        if(board.get(target) != null && (board.get(target).color.equals(this.color)))	return false;
        ret[target[0]][target[1]] = true;
        if(board.get(target) != null && !(board.get(target).color.equals(this.color)))	return false;
        return true;
    }

    /**
     * @return string name
     */
    public String toString() {
        return this.color.charAt(0) + "B";
    }


}
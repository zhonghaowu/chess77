package piece;
import chess.Board;
import chess.Player;
import chess.Assembly;

/**
 * This is the Knight
 * @author Zhonghao Wu Thomas Cooke
 *
 */

public class Knight extends Piece{
	/**
	 * 
	 * @param color color of knight
	 * @param owner owner of knight
	 * @param loc location of knight
	 * @param type must be knight
	 */
    public Knight(String color, Player owner, int[] loc, String type) {
        super(color, owner, loc, type);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean[][] legalMove(Board board) {
        // TODO Auto-generated method stub
        boolean[][] ret = new boolean[8][8];
        int[][] moves = {{loc[0]-2, loc[1]-1},
                        {loc[0]-1, loc[1]-2},
                        {loc[0]+1, loc[1]-2},
                        {loc[0]+2, loc[1]-1},
                        {loc[0]-1, loc[1]+2},
                        {loc[0]-2, loc[1]+1},
                        {loc[0]+1, loc[1]+2},
                        {loc[0]+2, loc[1]+1}
                    };
        for (int[] current : moves) {
            if(Assembly.inMatrix(current) &&
                    !(board.get(current) != null && board.get(current).color.equals(owner.color))) {
                ret[current[0]][current[1]] = true;
            }
        }
        return ret;
    }

    /**
     * @return string name
     */
    public String toString() {
        return this.color.charAt(0) + "N";
    }


}


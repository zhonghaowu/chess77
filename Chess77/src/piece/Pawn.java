package piece;

import java.util.Arrays;

import chess.*;

/**
 * This is the Pawn 
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Pawn extends Piece{
	/**
	 * counter for pawn
	 * will change each term
	 */
	public int pawnCounter = 0;
	/**
	 * counter for the two_step_move state of a pawn
	 * -1: never do a two_step move
	 * n: n equals the value of pawn counter when it do two_step_move
	 */
	int two_step_counter = -1;

	@Override
	public
	boolean[][] legalMove(Board board) {
		// TODO Auto-generated method stub
		boolean[][] ret = new boolean[8][8];
		int[] frontCoordinate = Assembly.advance(loc, color, 1);
		Piece frontPiece = board.get(frontCoordinate);
		if(frontPiece == null)	{
			if(Assembly.inMatrix(frontCoordinate)) ret[frontCoordinate[0]][frontCoordinate[1]] = true;
			int[] front2Coordinate = Assembly.advance(loc, color, 2);
			if(board.get(front2Coordinate) == null && !isMoved())	
				if(Assembly.inMatrix(front2Coordinate)) 
					ret[front2Coordinate[0]][front2Coordinate[1]] = true;
		}
		int[] possible1; int[] possible2;
		if(color.equals("white")) {
			possible1 = new int[] {loc[0] - 1, loc[1] - 1};
			possible2 = new int[] {loc[0] - 1, loc[1] + 1};
		}else {
			possible1 = new int[] {loc[0] + 1, loc[1] - 1};
			possible2 = new int[] {loc[0] + 1, loc[1] + 1};
		}
		if(Assembly.inMatrix(possible1) && board.get(possible1) != null && !board.get(possible1).color.equals(this.color))	
			if(Assembly.inMatrix(possible1)) ret[possible1[0]][possible1[1]] = true;
		if(Assembly.inMatrix(possible2) && board.get(possible2) != null && !board.get(possible2).color.equals(this.color))	
			if(Assembly.inMatrix(possible2)) ret[possible2[0]][possible2[1]] = true;
		return ret;
	}
	
	/**
	 * 
	 * @param color of pawn
	 * @param owner of pawn
	 * @param loc of pawn
	 * @param type must be pawn
	 */
	public Pawn(String color, Player owner, int[] loc, String type) {
		super(color, owner, loc, type);
	}

	@Override
	public boolean move(Board board, int[] end) {
		// TODO Auto-generated method stub
		if(!Assembly.inMatrix(end))	return false;
		if(Enpassant(board, end))	return true;
		boolean[][] lm = legalMove(board);
		if(lm[end[0]][end[1]]) {
			if(!king_safe_check(board,end))	return false;
			if(Math.abs(this.loc[0] - end[0]) == 2)	this.two_step_counter = this.pawnCounter;
			board.set(loc, null);
			board.set(end, this);
			this.loc = end;
			return true;
		}
		return false;
	}
	/**
	 * do Enpassant if target make Enpassant
	 * return true if did Enpassant
	 * @param board
	 * @param end
	 * @return true if pawn do Enpssant
	 */
	boolean Enpassant(Board board, int[] end) {
		int[] possible1 = null;
		int[] possible2 = null;

		if(this.color == "white") {
			possible1 = new int[] {loc[0] - 1, loc[1] - 1};
			possible2 = new int[] {loc[0] - 1, loc[1] + 1};
		}else {
			possible1 = new int[] {loc[0] + 1, loc[1] - 1};
			possible2 = new int[] {loc[0] + 1, loc[1] + 1};
		}

		if(Assembly.isSameLoc(end, possible1) || Assembly.isSameLoc(end, possible2)) {
			int[] possible3 = new int[] {loc[0], end[1]};
			Piece target = board.board[possible3[0]][possible3[1]];
			if(target == null)	return false;
			if(target.counter != 1)	return false;
			if(target.type.equals("pawn") && !target.color.equals(color))	{
				Pawn p = (Pawn)target;
				if(p.pawnCounter != p.two_step_counter)	return false;
				//this.owner.another.pieces.remove(target);
				board.board[possible3[0]][possible3[1]] = null;
				board.board[loc[0]][loc[1]] = null;
				this.loc = end;
				board.board[end[0]][end[1]] = this;
				if(!king_safe_check(board,end))	return false;
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @return string name
	 */
	public String toString() {
		return this.color.charAt(0) + "P";
	}
	
}

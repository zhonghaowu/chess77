package piece;


import chess.Board;
import chess.Player;
import chess.Assembly;

/**
 * This is the King
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class King extends Piece{
	/**
	 * 
	 * @param color color of king
	 * @param owner owner of king
	 * @param loc location of king
	 * @param type must be of king
	 */
	public King(String color, Player owner, int[] loc, String type) {
		super(color, owner, loc, type);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean[][] legalMove(Board board) {
		// TODO Auto-generated method stub
		boolean[][] ret = new boolean[8][8];
		for(int i = loc[0] - 1; i <= loc[0] + 1; i++) {
			for(int j = loc[1] - 1; j <= loc[1] + 1; j++) {
				int[] current = new int[] {i,j};
				if(Assembly.inMatrix(current) && !(board.get(current) != null && board.get(current).color.equals(owner.color))) {
					ret[i][j] = true;
				}
			}
		}
		return ret;
	}
	
	/**
	 * @return string name
	 */
	public String toString() {
		return this.color.charAt(0) + "K";
	}
	
	public boolean move(Board board, int[] end) {
		if(castling(board,end))	return true;
		// TODO Auto-generated method stub
		if(!Assembly.inMatrix(end))	return false;
		boolean[][] available = this.legalMove(board);
		if(!available[end[0]][end[1]])	return false;
		if(this.check_detection(board, end))	return false;
		board.board[end[0]][end[1]] = this;
		board.board[loc[0]][loc[1]] = null;
		this.loc = end;
		return true;
	}
	
	/**
	 * 
	 * @param board chess board
	 * @param end target coordinate
	 * @return true if and only if king move to int[]end will be checked
	 */
	public boolean check_detection(Board board, int[] end) {
		Board copy = board.getCopy();
		int[] oriloc = this.loc;	//store original loc
		copy.board[oriloc[0]][oriloc[1]] = null;
		copy.board[end[0]][end[1]] = this;
		this.loc = end;
		boolean[][] threaten = Assembly.threatenMatrix(copy, this.owner);
		boolean flag = false;
		if(threaten[end[0]][end[1]]) {
			flag = true;
		}
		this.loc = oriloc;
		return flag;
	}
	
	/**
	 * do castling if it's legal castling
	 * @param board chess board
	 * @param end end coordinate
	 * @return facticity if king is doing legal castling
	 */
	public boolean castling(Board board, int[] end) {
		if(!((color.equals("white") && end[0] == 7) || (color.equals("black") && end[0] == 0)))	return false;
		if(this.isMoved())	return false;
		if(Math.abs(loc[1] - end[1]) != 2)	return false;
		Rook rook = null;
		Piece piece = null;
		if(Assembly.isSameLoc(end, new int[] {loc[0], 6})) piece = board.get(new int[] {loc[0],7});
		else if(Assembly.isSameLoc(end, new int[] {loc[0], 2})) piece = board.get(new int[] {loc[0],0});		
		if(piece == null || !piece.type.equals("rook"))	return false;
		rook = (Rook) piece;
		if(rook.isMoved())	return false;
		int dev = end[1] - loc[1];
		int[] vec = loc;
		for(int i = 0; i <= 2; i++){
			if(!Assembly.isSameLoc(loc, vec) && board.get(vec) != null)	return false;
			if(check_detection(board,vec))	return false;
			if(dev < 0)	vec = new int[] {vec[0], vec[1]-1};
			else vec = new int[] {vec[0], vec[1]+1};
		}
		if(rook.loc[1] == 0 && board.board[0][1] != null) return false;
		board.board[loc[0]][loc[1]] = null;
		board.board[end[0]][end[1]] = this;
		this.loc = end;
		board.board[rook.loc[0]][rook.loc[1]] = null;
		if(Assembly.isSameLoc(end, new int[] {loc[0], 6})) {
			board.board[loc[0]][5] = rook;
			rook.loc = new int[] {loc[0],5};
		}
		else if(Assembly.isSameLoc(end, new int[] {loc[0], 2})) {
			board.board[loc[0]][3] = rook;
			rook.loc = new int[] {loc[0],3};
		}
		return true;
	}

}

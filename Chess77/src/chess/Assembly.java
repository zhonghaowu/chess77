package chess;

import java.util.ArrayList;
import java.util.Arrays;

import piece.Piece;

/**
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Assembly {
	/**
	 * map chess Coordinate like "a8" to matrix coordinate
	 * a8 (x,y) to (0,0)
	 * @author Zhonghao Wu Thomas Cooke
	 * @param str find the coordinate of a chess representation
	 * @return coordinate vector
	 */
	/**
	 * Tool class for static method,
	 * constructor is useless
	 */
	public Assembly() {}
	public static int[] toCoordinate(String str) {
		int y = str.charAt(0) - 'a';
		int x = 8 - (str.charAt(1) - '0');
		return new int[] {x,y};
	}
	
	/**
	 * return the coordinate of an line input
	 * @param line line of input
	 * @return 2 x 2 matrix ret[0] represent begin spot, ret[1] = end spot
	 */
	public static int[][] retCoordinate(String line) {
		line = line.trim();
		String[] inputs = line.split(" ");
		if(inputs.length < 2)	return new int[2][2];
		int[] a = toCoordinate(inputs[0]);
		int[] b = toCoordinate(inputs[1]);
		int[][] ret = new int[2][];
		ret[0] = a;
		ret[1] = b;
		return ret;
	}
	
	/**
	 * parse input line
	 * all possible inputs:
	 * e2 e4				return normal
	 * g7 g8 N 				return N
	 * resign				return resign
	 * g1 f3 draw?			return draw?
	 * draw					return draw
	 * @param line input String line
	 * @return return status string
	 */
	public static String stateParse(String line) {
		line = line.trim();
		String[] inputs = line.split(" ");
		if(inputs.length == 1) {
			if(inputs[0].equals("draw"))	return "draw";
			else	return "resign";
		}else if(inputs.length == 2) {
			return "normal";
		}else {
			return inputs[2];
		}
	}
	
	/**
	 * return a empty board for next step drawing
	 * @return String[][] empty board with column number
	 */
	public static String[][] emptyBoard() {
		String[][] rough = new String[8][9];
		for(int i = 0 ; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if((i+j) % 2 == 0)	rough[i][j] = "  ";
				else	rough[i][j] = "##";
			}
			rough[i][8] = "" + (8 - i);
		}
		return rough;
	}
	
	public static void draw(Board pieces) {
		String[][] output = emptyBoard();
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if(pieces.board[i][j] != null)	output[i][j] = pieces.board[i][j].toString();
			}
		}
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 9; j++) {
				System.out.print(output[i][j] + " ");
			}
			System.out.println();
		}
		for(int i = 0; i < 7; i++) System.out.print(" " + (char)(i + 'a') + " ");
		System.out.print(" h");
		System.out.println();
		System.out.println();
	}
	/*
	public static void main(String[] agrs) {
		ArrayList<String> arr = new ArrayList<>();
		arr.add("e2 e4");
		arr.add("g7 g8 N");
		arr.add("resign");
		arr.add("g1 f3 draw?");
		arr.add("draw");
		for(String s : arr) {
			for(int[] ax : retCoordinate(s)) {
				System.out.println(Arrays.toString(ax));
			}
			System.out.println(stateParse(s));
			System.out.println("---------------");
		}
		draw(new Board());
	}
	*/
	/**
	 * test if vector a and vector b hold same value
	 * @param a location a
	 * @param b location b
	 * @return boolean if vector a and vector b hold same value
	 */
	public static boolean isSameLoc(int[] a, int[] b) {
		return (a[0] == b[0]) && (a[1] == b[1]);
	}
	
	/**
	 * test if a coordinate is in 8x8 matrix
	 * @param coordinate coordinate you want to test
	 * @return boolean if a coordinate is in 8x8 matrix
	 */
	public static boolean inMatrix(int[] coordinate) {
		if(coordinate[0] >= 0 && coordinate[0] < 8 && coordinate[1] >= 0 && coordinate[1] < 8 )	return true;
		else	return false;
	}
	
	
	/**
	 * find the destination of a piece which 
	 * advance step step
	 * @param start initial position
	 * @param color color of the piece
	 * @param step step of advance
	 * @return return the location after advanced step units from start
	 */
	public static int[] advance(int[] start, String color, int step) {
		if(color.equals("white"))	return new int[] {start[0] - step, start[1]};
		else return new int[] {start[0] + step, start[1]};
	}
	
	/**
	 * give a boolean matrix that store all coordinate 
	 * that enemy can attack
	 * @param board chess board
	 * @param p player who is under threaten
	 * @return boolean[][] threaten matrix
	 */
	public static boolean[][] threatenMatrix(Board board, Player p){
		boolean[][] tm = new boolean[8][8];
		ArrayList<Piece> all = p.another.getPieces(board);
		for(Piece current : all) {
			if(current.type.equals("pawn")) {
				int[] front = advance(current.loc, current.color, 1);
				int[] a = new int[] {front[0], front[1] - 1};
				int[] b = new int[] {front[0], front[1] + 1};
				if(inMatrix(a) && !(board.get(a) != null && board.get(a).color.equals(current.color)))	tm[a[0]][a[1]] = true;
				if(inMatrix(b) && !(board.get(b) != null && board.get(b).color.equals(current.color)))	tm[b[0]][b[1]] = true;
				continue;
			}
			boolean[][] currentLegal = current.legalMove(board);
			for(int i = 0; i < 8; i++) {
				for(int j = 0; j < 8; j++) {
					if(currentLegal[i][j])	tm[i][j] = true;
				}
			}
		}
		return tm;
	}
}

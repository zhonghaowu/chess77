package chess;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import piece.*;
/**
 * 
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Chess {
	/**
	 * player who should move
	 */
	Player inAction;
	/**
	 * counter for checking draw requirement
	 */
	int globalCounter = 0;
	/**
	 * representing draw the time a player offering draw
	 */
	private int under_draw_require = 0;
	/**
	 * no parameter is needed for this class
	 */
	public Chess() {}
	public static void main(String[] agrs) {
		Player pw = new Player("white");
		Player pb = new Player("black");
		pw.another = pb;
		pb.another = pw;
		Board board = new Board();
		init(pw, pb, board);
		Chess chess = new Chess();
		chess.inAction = pw;
		gameLogic(pw, pb, board, chess);
	}
	
	/**
	 * logic of chess game
	 * @param pw
	 * @param pb
	 * @param board
	 * @param chess
	 */
	private static void gameLogic(Player pw, Player pb, Board board, Chess chess) {
		Scanner scanner = new Scanner(System.in);

		while(true) {
			stalemate_detection(chess.inAction,board);
			for(Piece p : chess.inAction.getPieces(board))	if(p.type.equals("pawn") && p.owner == chess.inAction)	((Pawn)p).pawnCounter++;
			chess.globalCounter++;
			Assembly.draw(board);
			int[][] moveMatrix = new int[2][2];
			String state = null;
			//loop forever until input is correct
			while(true) {
				state = chess.askInput(scanner, moveMatrix, chess);
				Piece p = board.get(moveMatrix[0]);
				if(state.equals("resign")) {
					System.out.println("resign\n");
					if(chess.inAction.another.color.equals("white"))	System.out.println("White wins");
					else System.out.println("Black wins");
					System.exit(0);
				}else if(state.equals("draw") && chess.under_draw_require + 1 == chess.globalCounter) {
					System.out.println("draw");
					System.exit(0);
				}else if(state.equals("N") || state.equals("B") || state.equals("R")) {
					if(promotion(p,board,moveMatrix[1],state))	break;
				}
				if(p == null) {illegalAnnouncement(); continue;}
				if(p.owner != chess.inAction)	{illegalAnnouncement(); continue;}
				if(state.equals("normal") || state.equals("draw?")) {
					if(p.move(board, moveMatrix[1])) {
						//move success
						p.counter++;
						if(state.equals("draw?"))	chess.under_draw_require = chess.globalCounter;
						promotion(p,board,moveMatrix[1],state);
						break;
					}
					else {illegalAnnouncement(); continue;}
				}
			}
			//win check
			if(!haveKing(chess.inAction.another, board)) {
				if(chess.inAction == pw)	System.out.println("White wins");
				else	System.out.println("Black wins");
				System.exit(0);
			}
			checkmate_detection(chess.inAction.another,board);
			if(check_detection(chess.inAction.another, board))	System.out.println("Check\n");
			if(chess.inAction == pw)	chess.inAction = pb;
			else	chess.inAction = pw;
		}
	}
	
	/**
	 * promotion
	 * @param p
	 * @param board
	 * @param end
	 * @param type
	 * @return true if and only if promotion occurs
	 */
	
	public static boolean promotion(Piece p, Board board, int[] end, String type) {
		if(!p.type.equals("pawn"))	return false;
		if((p.color.equals("white") && end[0] == 0) || (p.color.equals("black") && end[0] == 7)) {
			board.board[p.loc[0]][p.loc[1]] = null;
			if(type.equals("B")) {
				board.board[end[0]][end[1]] = new Bishop(p.color, p.owner, end, "bishop");
			}else if(type.equals("N")) {
				board.board[end[0]][end[1]] = new Knight(p.color, p.owner, end, "knight");
			}else if(type.equals("R")) {
				board.board[end[0]][end[1]] = new Rook(p.color, p.owner, end, "rook");
			}else {
				board.board[end[0]][end[1]] = new Queen(p.color, p.owner, end, "queen");
			}
			return true;
		}
		return false;
	}
	
	/**
	 * distribute pieces to each player
	 * @param pw
	 * @param pb
	 * @param board
	 */
	private static void init(Player pw, Player pb, Board board) {
		//distribute pawns
		for(int i = 0; i < 8; i++) {
			Pawn newPawn = new Pawn("white",  pw, new int[] {6, i}, "pawn");
			//pw.pieces.add(newPawn);
			board.board[6][i] = newPawn;
		}
		for(int i = 0; i < 8; i++) {
			Pawn newPawn = new Pawn("black",  pb, new int[] {1, i}, "pawn");
			//pb.pieces.add(newPawn);
			board.board[1][i] = newPawn;
		}
		//distribute kings
		King newKing = new King("white", pw, new int[] {7,4}, "king");
		King newKing2 = new King("black", pb, new int[] {0,4}, "king");
		board.board[7][4] = newKing;
		board.board[0][4] = newKing2;
		//distribute queens
		board.board[7][3] = new Queen("white", pw, new int[] {7,3}, "queen");
		board.board[0][3] = new Queen("black", pb, new int[] {0,3}, "queen");
		//distribute bishops
		board.board[7][2] = new Bishop("white", pw, new int[] {7,2}, "bishop");
		board.board[7][5] = new Bishop("white", pw, new int[] {7,5}, "bishop");
		board.board[0][2] = new Bishop("black", pb, new int[] {0,2}, "bishop");
		board.board[0][5] = new Bishop("black", pb, new int[] {0,5}, "bishop");
		//distribute knights
		board.board[7][1] = new Knight("white", pw, new int[] {7,1}, "knight");
		board.board[7][6] = new Knight("white", pw, new int[] {7,6}, "knight");
		board.board[0][1] = new Knight("black", pb, new int[] {0,1}, "knight");
		board.board[0][6] = new Knight("black", pb, new int[] {0,6}, "knight");
		//distribuete rook
		board.board[7][0] = new Rook("white", pw, new int[] {7,0}, "rook");
		board.board[7][7] = new Rook("white", pw, new int[] {7,7}, "rook");
		board.board[0][0] = new Rook("black", pb, new int[] {0,0}, "rook");
		board.board[0][7] = new Rook("black", pb, new int[] {0,7}, "rook");
		
		
	}
	/**
	 * 
	 * @param pw
	 * @param pb
	 * @param board
	 */
	private static void test_init(Player pw, Player pb, Board board) {

//		board.board[1][0] = new Pawn("black", pb, new int[] {1,0}, "pawn");
//		board.board[2][1] = new Pawn("black", pb, new int[] {2,1}, "pawn");
//		board.board[1][7] = new Pawn("black", pb, new int[] {1,7}, "pawn");
//		board.board[0][7] = new King("black", pb, new int[] {0,7}, "king");
//		board.board[6][5] = new King("white", pw, new int[] {6,5}, "king");
//		board.board[5][0] = new Bishop("white", pw, new int[] {5,0}, "bishop");
//		board.board[7][0] = new Bishop("white", pw, new int[] {7,0}, "bishop");
		board.board[7][2] = new King("black", pb, new int[] {7,2}, "king");
		board.board[5][1] = new Pawn("white", pw, new int[] {5,1}, "queen");
		board.board[5][3] = new King("white", pw, new int[] {5,3}, "king");
	}
	
	/**
	 * print illegal information
	 */
	public static void illegalAnnouncement() {
		System.out.println("Illegal move, try again\n");
	}
	
	//give inputed matrix value
	//return state String
	/**
	 * ask user for legal input
	 * @param scanner
	 * @param matrix
	 * @param chess
	 * @return
	 */
	private String askInput(Scanner scanner, int[][] matrix, Chess chess){
		String str = chess.inAction.color.substring(0,1).toUpperCase() + chess.inAction.color.substring(1) + "'s move: ";
		System.out.print(str);
		String line = scanner.nextLine();
		matrix[0] = Assembly.retCoordinate(line)[0];
		matrix[1] = Assembly.retCoordinate(line)[1];
		System.out.println();
		return Assembly.stateParse(line);
	}
	
	/**
	 * judge if a player have a king
	 * @param p
	 * @param board
	 * @return true if and only if Player p have a king
	 */
	private static boolean haveKing(Player p, Board board) {
		ArrayList<Piece> all = p.getPieces(board);
		for(Piece current : all)	if(current.type.equals("king"))	return true;
		return false;
	}
	
	/**
	 * get the king piece of Player p
	 * @param p in action player
	 * @param board chess board
	 * @return king king of the player
	 */
	public static King getKing(Player p, Board board) {
		ArrayList<Piece> all = p.getPieces(board);
		for(Piece current : all)	if(current.type.equals("king"))	return (King)current;
		return null;
	}
	
	/**
	 * check if player p is on check
	 * @param p
	 * @param board
	 * @return true if and only if Player p is on check
	 */
	private static boolean check_detection(Player p, Board board) {
		King k = getKing(p, board);
		return k.check_detection(board, k.loc);
	}
	
	/**
	 * check if Player p cause a stalemate,
	 * if p is on stalemate, print information and shut down programm
	 * the game
	 * @param p
	 * @param board
	 */
	private static void stalemate_detection(Player p, Board board) {
		if(check_detection(p,board))	return;
		ArrayList<Piece> all = p.getPieces(board);
		for(Piece current : all) {
			boolean[][] available = current.legalMove(board);
			for(int i = 0; i < 8; i++) {
				for(int j = 0; j < 8; j++) {
					if(available[i][j])	{
						if(current.type.equals("king")) {
							King k = (King)current;
							if(k.check_detection(board, new int[] {i,j}))	continue;
						}
						return;
					}
				}
			}
		}
		System.out.println("Stalemate");
		System.out.println("draw");
		System.exit(0);
		//is stalemate
	}
	
	/**
	 * check if player p is on checkmate, if yes, print result and shut the game
	 * @param p
	 * @param board
	 */
	private static void checkmate_detection(Player p, Board board) {
		if(!check_detection(p,board))	return;
		King k = getKing(p,board);
		boolean[][] lm = k.legalMove(board);
		boolean flag = false;
		//king move
		for(int i = k.loc[0] - 1; i <= k.loc[0] + 1; i++) {
			for(int j = k.loc[1] - 1; j <= k.loc[1] + 1; j++) {
				if(Assembly.inMatrix(new int[] {i,j}) && lm[i][j]) {
					if(k.check_detection(board, new int[] {i,j}))	continue;
					else {
						flag = true;
						break;
					}
				}
			}
		}
		//protector move
		
		for(Piece protector : p.getPieces(board)) {
			boolean[][] legal_move = protector.legalMove(board);
			int[] temp = protector.loc;
			for(int i = 0; i < 8; i++) {
				for(int j = 0; j < 8; j++) {
					Piece temp2 = board.get(new int[] {i,j});
					if(!legal_move[i][j])	continue;
					board.board[temp[0]][temp[1]] = null;
					board.board[i][j] = protector;
					protector.loc = new int[] {i,j};
					if(!check_detection(p, board)) flag = true;
					board.board[temp[0]][temp[1]] = protector;
					board.board[i][j] = temp2;
					protector.loc = temp;
				}
			}
		}
		if(!flag) {
			System.out.println("Checkmate\n");
			if(p.color.equals("white"))	System.out.println("Black wins");
			else System.out.println("White wins");
			System.exit(0);
		}
	}
}

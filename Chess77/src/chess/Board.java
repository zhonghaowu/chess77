package chess;

import piece.Piece;

/**
 * chess board containing a field board which store all the pieces(haven't been captured) with locations
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Board {
	/**
	 * 2D matrix of pieces of each player
	 */
	public Piece[][] board;
	/**
	 * create empty 8x8 2D matrix of Piece
	 */
	public Board() {
		board = new Piece[8][8];
	}
	
	/**
	 * get the piece referred by int[]coordinate
	 * @param coordinate coordinate you want to get
	 * @return target Piece
	 */
	public Piece get(int[] coordinate) {
		if(!Assembly.inMatrix(coordinate)) return null;
		else {
			return board[coordinate[0]][coordinate[1]];
		}
	}
	
	/**
	 * set board to input piece
	 * @param coordinate coordinate you want to set
	 * @param piece piece you want tp set
	 */
	public void set(int[] coordinate, Piece piece) {
		board[coordinate[0]][coordinate[1]] = piece;
	}
	
	/**
	 * return a copy of the board
	 * @return Board: copy of the board
	 */
	public Board getCopy(){
		Board copy = new Board();
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				copy.board[i][j] = this.board[i][j];
			}
		}
		return copy;
	}
}

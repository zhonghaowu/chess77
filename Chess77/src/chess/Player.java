package chess;

import java.util.ArrayList;
import java.util.Iterator;

import piece.Piece;

/**
 * Player class
 * hold color and Player object of another player
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Player {
	/**
	 * color of the pieces the player own
	 */
	public String color;
	/**
	 * enemy of the player
	 */
	public Player another;
	
	
	/**
	 * input a String of the color of the player
	 * @param color color of the player
	 */
	public Player(String color) {
		this.color = color;
		//pieces = new ArrayList<>();
	}
	
	/**
	 * get all the pieces of the player
	 * @param board chess board
	 * @return all the pieces of a player
	 */
	public ArrayList<Piece> getPieces(Board board){
		ArrayList<Piece> pieces = new ArrayList<>();
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if(board.board[i][j] != null && board.board[i][j].color.equals(this.color)) {
					pieces.add(board.board[i][j]);
				}
			}
		}
		return pieces;
	}
}
